%global maj 0
%{!?_pkgdocdir: %global _pkgdocdir %{_docdir}/%{name}-%{version}}

Name:       sord
Version:    0.16.14
Release:    1
Summary:    A lightweight Resource Description Framework (RDF) C library
License:    ISC
URL:        http://drobilla.net/software/sord/
Source0:    http://download.drobilla.net/%{name}-%{version}.tar.xz

BuildRequires: doxygen
BuildRequires: glib2-devel
BuildRequires: serd-devel >= 0.30.0
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: pcre-devel
BuildRequires: meson

%description
%{name} is a lightweight C library for storing Resource Description
Framework (RDF) data in memory. %{name} and parent library serd form 
a lightweight RDF tool-set for resource limited or performance critical 
applications.

%package devel
Summary:    Development libraries and headers for %{name}
Requires:   %{name}%{_isa} = %{version}-%{release}

%description devel
%{name} is a lightweight C library for storing Resource Description
Framework (RDF) data in memory.

This package contains the headers and development libraries for %{name}.

%prep
%autosetup -p1

%build
%meson
%meson_build

%install
%meson_install
install -d %{buildroot}%{_docdir}/%{name}
mv %{buildroot}%{_docdir}/%{name}-%{maj} %{buildroot}%{_docdir}/%{name}
cp ./AUTHORS %{buildroot}%{_docdir}/%{name}
cp ./NEWS %{buildroot}%{_docdir}/%{name}
cp ./README.md %{buildroot}%{_docdir}/%{name}

%check
%meson_test

%files
%{_pkgdocdir}
%exclude %{_pkgdocdir}/%{name}-%{maj}/
%license COPYING
%{_libdir}/lib%{name}-%{maj}.so.*
%{_bindir}/sordi
%{_bindir}/sord_validate
%{_mandir}/man1/%{name}*.1*

%files devel
%{_pkgdocdir}/%{name}-%{maj}/
%{_libdir}/lib%{name}-%{maj}.so
%{_libdir}/pkgconfig/%{name}-%{maj}.pc
%{_includedir}/%{name}-%{maj}/

%changelog
* Thu Feb 06 2025 Ge Wang <wang__ge@126.com> - 0.16.14-1
- update to 0.16.14

* Mon Jan 02 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 0.16.10-1
- update to 0.16.10

* Mon Jun 13 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 0.16.8-1
- Initial Packaging
